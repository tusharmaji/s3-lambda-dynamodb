

# Python Code
data "archive_file" "zip" {
  type        = "zip"
  source_file = "dynamodb_lambda.py"
  output_path = "dynamodb_lambda.zip"
}

# S3 Creation
resource "aws_s3_bucket" "bucket" {
  bucket = "my-lambda-test-bucket-987"
  tags = {
    Name        = "My bucket"
  }
}
resource "aws_s3_bucket_acl" "example" {
  bucket = aws_s3_bucket.bucket.id
  acl = "private"
}

# Role and Policy for Lambda
resource "aws_iam_policy" "admin_policy" {
  name        = "admin_policy"
  path        = "/"
  description = "admin policy"
  tags = {
    Name = "admin_policy"
  }
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        }
    ]
})
}
resource "aws_iam_role_policy_attachment" "role_policy_attach" {
  role       = aws_iam_role.iam_for_lambda.name
  policy_arn = aws_iam_policy.admin_policy.arn
}
resource "aws_iam_role" "iam_for_lambda" {
  name = "iam_for_lambda"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

# Creation of Lambda Function
resource "aws_lambda_function" "lambda" {
  function_name = "write_to_dynamodb_lambda"

  filename         = "${data.archive_file.zip.output_path}"
  source_code_hash = "${data.archive_file.zip.output_base64sha256}"

  role    = aws_iam_role.iam_for_lambda.arn
  handler = "dynamodb_lambda.lambda_handler"
  runtime = "python3.6"
  timeout = 30
  memory_size = 128

  environment {
    variables = {
      greeting = "Hello"
    }
  }
}

# S3 Trigger for Lambda
resource "aws_s3_bucket_notification" "aws-lambda-trigger" {
  bucket = aws_s3_bucket.bucket.id
  lambda_function {
    lambda_function_arn = aws_lambda_function.lambda.arn
    events              = ["s3:ObjectCreated:*"]
    filter_suffix       = ".json"
  }
}
resource "aws_lambda_permission" "test" {
  statement_id  = "AllowS3Invoke"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.lambda.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::${aws_s3_bucket.bucket.id}"
}

# Creation of DynamoDB
resource "aws_dynamodb_table" "example" {
  name             = "dynamodb-test"
  hash_key         = "emp_id"
  billing_mode   = "PROVISIONED"
  read_capacity  = 5
  write_capacity = 5
  attribute {
    name = "emp_id"
    type = "S"
  }
  ttl {
    attribute_name = "TimeToExist"
    enabled        = false
  }
}